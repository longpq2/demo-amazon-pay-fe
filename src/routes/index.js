import { lazy, Suspense } from "react"
import styled from "styled-components"
import {
  BrowserRouter,
  Routes,
  Route,
  Redirect
} from "react-router-dom"

import Header from "components/header"

const HomePage = lazy(() => import("pages/home"))
const CategoryPage = lazy(() => import("pages/category"))
const LoginPage = lazy(() => import("pages/auth/login"))
const RegisterPage = lazy(() => import("pages/auth/register"))


const PaymentPage = lazy(() => import("pages/payments"))
const PaymentSuccessPage = lazy(() => import("pages/payments/payment-success"))

const PaymentReviewPage = lazy(() => import("pages/payments/payment-review"))


const PaymentCancelPage = lazy(() => import("pages/payments/payment-cancel"))


const VideosPage = lazy(() => import('pages/videos'))

const NotFound = lazy(() => import("components/not-found"))

const AppContainer = styled.div`
  min-height: 100vh;
`

// const PrivateRoute = (Component, Layout) => {
//   const isLogin = localStorage.getItem('IS_USER_LOGIN')

//   if (!isLogin) {
//     return (
//       <Redirect to='/login' />
//     )
//   }

//   return (
//     <Route
//       path="/"
//       element={
//         <Suspense fallback={<>Loading...</>}>
//           <Layout>
//             <Component />
//           </Layout>
//         </Suspense>
//       }
//     />
//   )
// }

const AppRoutes = () => {
  return (
    <AppContainer>
      <Suspense fallback={<p>Loading...</p>}>
        <BrowserRouter>
          <Header />
          <Routes>
            <Route
              path="/"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <HomePage />
                </Suspense>
              }
            />
            <Route
              path="/login"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <LoginPage />
                </Suspense>
              }
            />
            <Route
              path="/register"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <RegisterPage />
                </Suspense>
              }
            />
            <Route
              path="/category"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <CategoryPage />
                </Suspense>
              }
            />

            <Route
              path="/videos"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <VideosPage />
                </Suspense>
              }
            />

            <Route
              path="/payments"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <PaymentPage />
                </Suspense>
              }
            />

            <Route
              path="/payments/success"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <PaymentSuccessPage />
                </Suspense>
              }
            />

            <Route
              path="/payments/review"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <PaymentReviewPage />
                </Suspense>
              }
            />

            <Route
              path="/payments/cancel"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <PaymentCancelPage />
                </Suspense>
              }
            />

            <Route
              path="*"
              element={
                <Suspense fallback={<>Loading...</>}>
                  <NotFound />
                </Suspense>
              }
            />
          </Routes>
        </BrowserRouter>
      </Suspense>
    </AppContainer>
  )
}

export default AppRoutes