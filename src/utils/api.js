import axios from 'axios'

const BASE_URL = process.env.REACT_APP_API_URL
const token = localStorage.getItem('TOKEN')

const instance = axios.create({
  baseURL: BASE_URL,
  timeout: 20000,
  headers: {'Authorization': `Bearer ${token}`}
})

class appApi {
  static onError(e) {
    console.log('======> appApi Error: ', e)
    // if (e.response.status === 404) {
    //   // alert(404)
    //   return
    // }
  }

  static get(url, payload) {
    return instance
      .get(`${BASE_URL}/${url}`, payload)
      .catch(error => appApi.onError(error))
  }

  static post(url, payload) {
    return instance
      .post(`${BASE_URL}/${url}`, payload)
      .catch(error => appApi.onError(error))
  }

  static put(url, payload) {
    return instance
      .put(`${BASE_URL}/${url}`, payload)
      .catch(error => appApi.onError(error))
  }

  static delete(url, payload) {
    return instance
      .delete(`${BASE_URL}/${url}`, payload)
      .catch(error => appApi.onError(error))
  }
}

export { appApi }