import { RecoilRoot } from 'recoil'

import Theme from './theme'
import Init from './app/init'
import Routes from './routes'
import './App.css'

function App() {
  return (
    <RecoilRoot>
      <Theme>
        <Init>
          <Routes />
        </Init>
      </Theme>
    </RecoilRoot>
  )
}

export default App
