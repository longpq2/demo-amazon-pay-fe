import { appApi } from 'utils/api'

const getAllUser = (payload) => {
  return appApi.get('api/user/list', payload)
}

const getUserById = (payload) => {
  return appApi.get(`api/user/${payload.userId}`, payload)
}

export {
  getAllUser,
  getUserById
}