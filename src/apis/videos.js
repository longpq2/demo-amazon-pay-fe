import { appApi } from 'utils/api'

const getAllVideo = (payload) => {
  return appApi.get('api/video/list', payload)
}

export {
  getAllVideo
}