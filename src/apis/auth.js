import { appApi } from 'utils/api'

const login = (payload) => {
  return appApi.post('api/auth/login', payload)
}

const register = (payload) => {
  return appApi.post('api/auth/register', payload)
}

export {
  login,
  register
}