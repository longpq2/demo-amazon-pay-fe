import { appApi } from 'utils/api'

const getAllPayment = (payload) => {
  return appApi.get('api/payment/list', payload)
}

const signAmazonPay = (payload) => {
  return appApi.get('api/payment/sign-amazon-pay', payload)
}

const checkoutSessionAmazonPay = (payload) => {
  return appApi.get('api/payment/checkout-session-amazon-pay', payload)
}

const getCheckoutSessionAmazonPayInfo = (payload) => {
  console.log('====> payload: ', payload)
  return appApi.get(`api/payment/checkout-session/info/${payload}`)
}

const updateCheckoutSessionAmazonPay = (sessionId, payload) => {
  console.log('====> payload: ', payload)
  return appApi.post(`api/payment/checkout-session/update/${sessionId}`, payload)
}

const checkoutSessionComplete = (sessionId) => {
  return appApi.post(`api/payment/checkout-session/complete/${sessionId}`)
}

// const sentPoint = (sessionId)

export {
  getAllPayment,
  signAmazonPay,
  checkoutSessionAmazonPay,
  getCheckoutSessionAmazonPayInfo,
  updateCheckoutSessionAmazonPay,
  checkoutSessionComplete
}