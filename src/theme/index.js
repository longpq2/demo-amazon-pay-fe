import React from 'react'
import { ThemeProvider } from 'styled-components'

const Theme = (props) => {
  const theme = {
    color: '#333'
  }

  return(
    <ThemeProvider theme={theme}>
      { props.children }
    </ThemeProvider>
  )
}

export default Theme