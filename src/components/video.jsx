import styled from "styled-components"

const VideoContent = styled.div`

`

const Player = ({ path }) => {
  return (
    <VideoContent>
      <video width="400" controls>
        <source src={path} type="video/*" />
      </video>
    </VideoContent>
  )
}

export default Player