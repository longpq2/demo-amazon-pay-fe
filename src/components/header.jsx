import { Link } from "react-router-dom"

const Header = () => {
  return (
    <nav>
      <Link to={'/'}>Home</Link>
      <Link to={'/category'}>Category</Link>
    </nav>
  )
}

export default Header