import { selector } from "recoil"

import { getAllVideoAction } from './actions'

const listVideoSelector = selector({
  key: 'listVideoSelector',

  get: async () => {
    const res = await getAllVideoAction()
    
    return res
  },

  set: () => {

  }
})

export { listVideoSelector }