import { atom } from 'recoil'

const videosAtom = atom({
  key: 'videosAtom',
  default: []
})

export { videosAtom }