import { getAllVideo } from 'apis/videos'

async function getAllVideoAction() {
  try {
    const res = await getAllVideo()
    if (!res.data.success) {
      throw "Error get all video"
    }

    return res.data.result
  } catch(error){
    console.log('_getAllVideo error: ', error)
  }
}

export { getAllVideoAction }