import { atom } from 'recoil'

const usersAtom = atom({
  key: 'usersState',
  default: []
})

export {
  usersAtom
}