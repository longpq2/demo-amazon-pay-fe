import { selector, selectorFamily } from 'recoil'

import { getAllUserAction, getUserByIdAction } from './actions'

const usersQuery = selector({
  key: 'getListUser',
  get: async () => {
    const result = await getAllUserAction()
    return result
  },
  set: async (func) => {
    console.log('=====> FUNC: ', func)
  }
})

const userDetailQuery = selectorFamily({
  key: 'getUserDetail',
  get: (id) => async () => {
    
    const result = await getUserByIdAction(id)
    console.log('====> RESULT: ', result)

    return result
  }
})

export { usersQuery, userDetailQuery }