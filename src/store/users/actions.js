import { getAllUser, getUserById } from 'apis/users'

async function getAllUserAction() {
  try {
    const res = await getAllUser()
    if (!res.data.success) {
      throw "Error get all user"
    }

    return res.data.result
  } catch(error){
    console.log('_getAllUser error: ', error)
  }
}

async function getUserByIdAction(userId) {
  try {
    const res = await getUserById({ userId })
    if (!res.data.success) {
      throw "Error get all user"
    }

    return res.data.result
  } catch(error){
    console.log('_getAllUser error: ', error)
  }
}

export { getAllUserAction, getUserByIdAction }