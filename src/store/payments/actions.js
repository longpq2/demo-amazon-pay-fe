import {
  getAllPayment,
  signAmazonPay,
  checkoutSessionAmazonPay,
  getCheckoutSessionAmazonPayInfo,
  updateCheckoutSessionAmazonPay
} from 'apis/payments'

async function getAllPaymentAction() {
  try {
    const res = await getAllPayment()

    if (!res.data.success) {
      throw "Error get all payment"
    }

    return res.data.result
  } catch(error){
    console.log('getAllPayment error: ', error)
  }
}

async function signAmazonPayAction() {
  try {
    const res = await signAmazonPay()

    if (!res.data.success) {
      throw "Error sign Amazon Pay"
    }

    return res.data.result

  } catch(error) {
    console.log('signAmazonPay error: ', error)
  }
}

async function checkoutSessionAmazonPayAction(sessionId) {
  try {
    const res = await checkoutSessionAmazonPay()

    console.log('=====> checkoutSessionAmazonPayAction res: ', res)

    if (!res.data.success) {
      throw "Error sign Amazon Pay"
    }

    return res.data.result

  } catch(error) {
    console.log('signAmazonPay error: ', error)
  }
}

async function getCheckoutSessionAmazonPayInfoAction(sessionId) {
  try {
    const res = await getCheckoutSessionAmazonPayInfo(sessionId)

    if (!res.data.success) {
      throw "Error sign Amazon Pay"
    }

    return res.data.result

  } catch(error) {
    console.log('signAmazonPay error: ', error)
  }
}

async function updateCheckoutSessionAmazonPayAction(sessionId, payload) {
  try {
    const res = await updateCheckoutSessionAmazonPay(sessionId, payload)

    if (!res.data.success) {
      throw "Error sign Amazon Pay"
    }

    return res.data.result

  } catch(error) {
    console.log('signAmazonPay error: ', error)
  }
}

export {
  getAllPaymentAction,
  signAmazonPayAction,
  checkoutSessionAmazonPayAction,
  getCheckoutSessionAmazonPayInfoAction,
  updateCheckoutSessionAmazonPayAction
}