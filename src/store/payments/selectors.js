import { selector, selectorFamily } from "recoil"

import {
  getAllPaymentAction,
  signAmazonPayAction,
  checkoutSessionAmazonPayAction,
  getCheckoutSessionAmazonPayInfoAction,
  updateCheckoutSessionAmazonPayAction
} from './actions'

import { paymentKey } from "./atom"

const listPaymentSelector = selector({
  key: 'listPaymentSelector',

  get: async () => {
    return await getAllPaymentAction()
  }
})

const signAmazonPaySelector = selector({
  key: 'signAmazonPaySelector',

  get: async () => {
    return await signAmazonPayAction()
  }
})

const checkoutSessionAmazonPaySelector = selector({
  key: 'checkoutSessionAmazonPaySelector',

  get: async () => {
    return await checkoutSessionAmazonPayAction()
  }
})

const checkoutSessionAmazonPayInfoSelector = selectorFamily({
  key: 'checkoutSessionAmazonPayInfoSelector',

  get: (sessionId) => async () => {
    return await getCheckoutSessionAmazonPayInfoAction(sessionId)
  }
})

const updateCheckoutSessionAmazonPaySelector = selectorFamily({
  key: 'updateCheckoutSessionAmazonPaySelector',
  get: (sessionId, payload) => async () => {
    return await updateCheckoutSessionAmazonPayAction({ sessionId, payload })
  }
})

const updatePaymentKey = selector({
  key: 'tempCelsius',
  get: ({get}) => (get(paymentKey)),
  set: ({set}, newValue) => set(paymentKey, newValue),
});


export {
  listPaymentSelector,
  signAmazonPaySelector,
  checkoutSessionAmazonPaySelector,
  checkoutSessionAmazonPayInfoSelector,
  updateCheckoutSessionAmazonPaySelector,
  updatePaymentKey
}