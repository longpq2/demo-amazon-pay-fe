import { atom } from 'recoil';

const paymentKey = atom({
  key: 'paymentKey',
  default: 32,
})

export { paymentKey } 