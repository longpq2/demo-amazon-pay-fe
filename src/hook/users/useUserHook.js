import { useEffect } from "react"
import { getAllUser } from 'apis/users'

const useUserHook = () => {
  let users

  useEffect(() => {
    fetchData()
  }, [])

  const fetchData = async () => {
    const res = await getAllUser()
    // if (!res.success) {
    //   throw 'Error get users'
    // }

    console.log('===> RES: ', res)

    const { data } = res.data

    console.log('===> DATATA: ', data)

    users = data
  }


  return users
}

export default useUserHook