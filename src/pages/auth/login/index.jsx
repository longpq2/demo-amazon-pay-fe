import { Button, Form, Input } from 'antd';
import 'antd/dist/antd.css';

import { login } from 'apis/auth'

const LoginPage = () => {
  // const submit = useSubmit()

  const onFinish = async (values) => {
    const res = await login(values)

    if (!res.data.success) {
      console.log('Error login!')
      return
    }

    localStorage.setItem('TOKEN', res.data.result.access_token)
  }

  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  )
}

export default LoginPage