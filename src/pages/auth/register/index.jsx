import { Button, Form, Input } from 'antd';
import 'antd/dist/antd.css';

import { register } from 'apis/auth'

const RegisterPage = () => {
  const onRegister = async (values) => {
    const res = await register({ ...values, avatar: null })

    if (!res.success) {
      alert('Register failure!')
      return
    }

    alert('Register successfully!')
  }

  const onFinishFailed = (errors) => {
    alert('error')
  }

  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onRegister}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        label="Email"
        name="email"
      >
        <Input />
      </Form.Item>
      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Register
        </Button>
      </Form.Item>
    </Form>
  )
}

export default RegisterPage