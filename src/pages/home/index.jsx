import { useRecoilValue } from 'recoil'
import 'antd/dist/antd.css';

import { usersQuery } from 'store/users/selectors'
// import { usersAtom } from 'store/users/atoms'

import { HomeContainer, Content } from './styled/index.styled'

const HomePage = () => {
  const users = useRecoilValue(usersQuery)
  // const usersss = useRecoilValue(usersAtom)

  return (
    <HomeContainer>
      {users.map((user, index) => (
        <Content key={`${user.username}-${index}`}>
          <h3>{user.username}</h3>
          <p>{user.email}</p>
        </Content>
      ))}
    </HomeContainer>
  )
}

export default HomePage