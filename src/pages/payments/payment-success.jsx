import styled from "styled-components"
import { useEffect } from "react"
import queryString from 'query-string'
import { useLocation } from "react-router-dom"

import { useRecoilValue } from 'recoil'

// import { sentPoint } from 'apis/payments'

import { paymentKey } from 'store/payments/atom'

import { 
  getCheckoutSessionAmazonPayInfo,
  checkoutSessionComplete
} from 'apis/payments'



const SuccessContainer = styled.div`

`

const PaymentSuccess = () => {
  const ran = localStorage.getItem('ID_P')
  const location = useLocation()


  useEffect(() => {
    const getData = async () => {
      const { amazonCheckoutSessionId } = queryString.parse(location.search)

      const res = await checkoutSessionComplete(amazonCheckoutSessionId)

      console.log('======> checkoutSessionComplete: ', res)
    }
    getData()
  }, [])


  return (
    <SuccessContainer>
      <h1>Payment success</h1>
    </SuccessContainer>
  )
}

export default PaymentSuccess