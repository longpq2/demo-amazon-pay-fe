import { useEffect } from 'react'
import { useRecoilValue, useRecoilState } from 'recoil'

import styled from 'styled-components'
import 'antd/dist/antd.css'

import {
  listPaymentSelector,
  signAmazonPaySelector,
  // checkoutSessionAmazonPaySelector
} from 'store/payments/selectors'

import { updatePaymentKey } from 'store/payments/selectors'

const PaymentContainer = styled.div`
  margin-top: 50px;
`

const Content = styled.div`

`

const PaymentPage = () => {
  const payments = useRecoilValue(listPaymentSelector)
  const signAmazonPay = useRecoilValue(signAmazonPaySelector)

  const [key, setKey] = useRecoilState(updatePaymentKey);

  console.log('======> signAmazonPay: ', signAmazonPay )

  // if(signAmazonPay) {
  //   setKey(signAmazonPay.randomId)
  // }

  useEffect(() => {
    const ran = Math.random()

    localStorage.setItem('ID_P', `${signAmazonPay.randomId}`)

    _renderButton()
  }, [])

  const _renderButton = async () => {
    console.log('====>  window.amazon: ',  window.amazon)
    if (window.amazon && signAmazonPay) {
      const payload = {
        webCheckoutDetails: signAmazonPay.payload.webCheckoutDetails,
        storeId: signAmazonPay.payload.storeId,
      }

      window.amazon.Pay.renderButton('#AmazonPayButton', {
        // set checkout environment
        merchantId: process.env.REACT_APP_AMAZON_MERCHANT_ID,
        publicKeyId: process.env.REACT_APP_AMAZON_PUBLIC_KEY,
        ledgerCurrency: 'USD',
        // customize the buyer experience
        checkoutLanguage: 'en_US',
        productType: 'PayOnly',
        placement: 'Checkout',
        buttonColor: 'Gold',
        // estimatedOrderAmount: { "amount": "1.00", "currencyCode": "USD" },
        // configure Create Checkout Session request
        createCheckoutSessionConfig: {
          payloadJSON: JSON.stringify(payload), // string generated in step 2
          signature: `${signAmazonPay.signature}` // signature generated in step 3
        }
      })
    }
  }

  return (
    <PaymentContainer>
      {payments.map((payment, index) => (
        <Content key={`${payment.id}-${index}`}>
          <h3>Points: {payment.points}</h3>
          <p>Yen prices: {payment.yen_prices}</p>
        </Content>
      ))}
      <div id="AmazonPayButton"></div>
    </PaymentContainer>
  )
}

export default PaymentPage