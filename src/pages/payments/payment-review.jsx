import { useRecoilValue } from "recoil"
import styled from "styled-components"
import { useLocation, useNavigate } from "react-router-dom"
import queryString from 'query-string'

import { checkoutSessionAmazonPayInfoSelector } from 'store/payments/selectors'

import { updateCheckoutSessionAmazonPay } from 'apis/payments'


const ReviewContainer = styled.div`

`

const PaymentReview = () => {
  const location = useLocation()
  const navigate = useNavigate()

  // const a = useRecoilValue()

  const { amazonCheckoutSessionId } = queryString.parse(location.search)

  const sessionInfo = useRecoilValue(checkoutSessionAmazonPayInfoSelector(amazonCheckoutSessionId))

  console.log('====> sessionInfo: ', sessionInfo)

  const onPlaceOrder = async () => {
    try {
      const payload = {
        // webCheckoutDetails: {
        //   // checkoutReviewReturnUrl: null,
        //   checkoutResultReturnUrl: 'http://localhost:3000/payments/success',
        // },
        paymentDetails: {
          paymentIntent: 'AuthorizeWithCapture',
          canHandlePendingAuthorization: false,
          chargeAmount: { amount: '1.00', currencyCode: 'USD' }
        },
        // merchantMetadata: {
        //   merchantReferenceId: "A1Z9XSQHTO18KK",
        //   merchantStoreName: "H2H",
        //   noteToBuyer: "Note to buyer",
        //   customInformation: "Custom information"
        // }
      }
      const res = await updateCheckoutSessionAmazonPay(sessionInfo.checkoutSessionId, { payload })
      console.log('====> resss: ', res)
      if (res.data.success) {
        window.location.replace(res.data.result.webCheckoutDetails.amazonPayRedirectUrl)
      }
    } catch (error) {
      console.log('===> error: ', error)
    }
  }

  return (
    <ReviewContainer>
      <h1>Payment review</h1>

      <button onClick={onPlaceOrder}>Place a order</button>
    </ReviewContainer>
  )
}

export default PaymentReview