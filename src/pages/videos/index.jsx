import { useRecoilValue } from "recoil"
import styled from "styled-components"

import { listVideoSelector } from 'store/videos/selectors'
import Player from "components/video"


const VideoContainer = styled.div`

`

const VideosPage = () => {

  const videos = useRecoilValue(listVideoSelector)

  console.log('====> VIDEOS: ', videos)

  return (
    <VideoContainer>
      <p>Video page</p>
      {videos.map((video) => (
        <Player key={`player-${video.id}`} path={video.path} />
      ))}
    </VideoContainer>
  )
}

export default VideosPage