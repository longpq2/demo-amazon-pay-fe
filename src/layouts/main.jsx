import styled from "styled-components"

const MainContainer = styled.div`

`

const MainLayout = (props) => {
  return (
    <MainContainer>
      {props.children}
    </MainContainer>
  )
}

export default MainLayout